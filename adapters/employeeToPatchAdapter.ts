import { IEmployee, IEmployeesWithDate } from '../models/IEmployee';

/**
 * @description This function adapts the employee to be updated to the format that the API expects
 */
export const employeeToPatchAdapter = (updatedEmployee: IEmployee, current: IEmployeesWithDate): IEmployeesWithDate => {
	const currentDate = new Date();

	const adaptedEmployee = {
		...updatedEmployee,
		id: current.id,
		charge: updatedEmployee.charge.name,
		area: updatedEmployee.area.name,
		updatedAt: currentDate,
		createdAt: current.createdAt,
	};

	return adaptedEmployee;
};
