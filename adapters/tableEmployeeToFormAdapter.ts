import { areasOptions, chargesOptions } from '@/data/selectOptions';
import { IEmployeeWithId, ISelectOption } from '@/models/IEmployee';

/**
 * @description Adapts the employee to be updated to the format that the form expects
 */
export const tableEmployeeToFormAdapter = (data: IEmployeeWithId): IEmployeeWithId => {
	const { charge, area } = data;
	const actualCharge = chargesOptions.find(option => option.name === (charge as unknown as string));
	const actualArea = areasOptions.find(option => option.name === (area as unknown as string));

	const adaptedData = {
		...data,
		charge: actualCharge as ISelectOption,
		area: actualArea as ISelectOption,
	};

	return adaptedData;
};
