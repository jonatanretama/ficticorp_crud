import { v4 as uuidv4 } from 'uuid';
import { IEmployee, IEmployeesWithDate } from '../models/IEmployee';

/**
 * @description Adapts the employee to be created to the format that the API expects
 */
export const employeeToPostAdapter = (newEmployee: IEmployee): IEmployeesWithDate => {
	const currentDate = new Date();

	const adaptedEmployee = {
		...newEmployee,
		id: uuidv4(),
		createdAt: currentDate,
		updatedAt: currentDate,
		charge: newEmployee.charge.name,
		area: newEmployee.area.name,
	};

	return adaptedEmployee;
};
