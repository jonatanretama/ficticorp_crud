import { EmployeeUpperEnum, IEmployeesWithDate } from '@/models/IEmployee';
import Papaparse from 'papaparse';

/**
 * @description Adapter to convert an array of employees to CSV data
 */
export const employeeJsonToCSV = (data: IEmployeesWithDate[]) => {
	const customColumnNames: Record<string, string> = {
		id: EmployeeUpperEnum.ID,
		name: EmployeeUpperEnum.NAME,
		paternalSurname: EmployeeUpperEnum.PATERNAL_SURNAME,
		maternalSurname: EmployeeUpperEnum.MATERNAL_SURNAME,
		email: EmployeeUpperEnum.EMAIL,
		phone: EmployeeUpperEnum.PHONE,
		charge: EmployeeUpperEnum.CHARGE,
		area: EmployeeUpperEnum.AREA,
		salary: EmployeeUpperEnum.SALARY,
		createdAt: EmployeeUpperEnum.CREATED_AT,
		updatedAt: EmployeeUpperEnum.UPDATED_AT,
	};

	const columnNames = Object.keys(customColumnNames);

	// Map the column names to a format compatible with Papaparse
	const columnNamesOrdered = columnNames.map(key => customColumnNames[key] as string);

	// Map the data to a format compatible with Papaparse
	const csvData = data.map((item: { [x: string]: any }) => {
		return columnNames.map(key => item[key] || '');
	});

	const config = {
		quotes: false,
		quoteChar: '"',
		escapeChar: '"',
		delimiter: ',',
		header: true,
		newline: '\r\n',
		skipEmptyLines: false,
	};

	const csv = Papaparse.unparse(
		{
			fields: columnNamesOrdered,
			data: csvData,
		},
		config
	);

	return csv;
};
