import { IEmployeeCSV, IEmployeesWithDate } from '@/models/IEmployee';
import Papaparse from 'papaparse';

/**
 * @description Adapter to convert CSV data to an array of employees
 */
export const csvToEmployeeAdapter = (csv: string): IEmployeesWithDate[] => {
	const parsed = Papaparse.parse(csv, { header: true });
	const dataParsed: IEmployeeCSV[] = parsed.data as IEmployeeCSV[];

	if (!dataParsed[0]?.NAME) {
		return [];
	}

	const adaptedEmployees = dataParsed.map(row => ({
		id: row.ID,
		name: row.NAME,
		paternalSurname: row.PATERNAL_SURNAME,
		maternalSurname: row.MATERNAL_SURNAME,
		email: row.EMAIL,
		phone: row.PHONE,
		charge: row.CHARGE,
		area: row.AREA,
		salary: row.SALARY,
		createdAt: row.CREATED_AT,
		updatedAt: row.UPDATED_AT,
	}));

	return adaptedEmployees;
};
