export enum AllowedChargesEnum {
	DesarrolladorSoftware = 'Desarrollador de Software',
	ArquitectoSoftware = 'Arquitecto de Software',
	Tester = 'Tester',
	DisenadorGrafico = 'Diseñador Gráfico',
	AnalistaRequerimientos = 'Analista de Requerimientos',
}
