export interface IEmployee {
	name: string;
	paternalSurname: string;
	maternalSurname: string;
	email: string;
	phone: string;
	charge: ISelectOption;
	area: ISelectOption;
	salary: string;
}

export interface IEmployeeCSV {
	ID: string;
	NAME: string;
	PATERNAL_SURNAME: string;
	MATERNAL_SURNAME: string;
	EMAIL: string;
	PHONE: string;
	CHARGE: string;
	AREA: string;
	SALARY: string;
	CREATED_AT: string;
	UPDATED_AT: string;
}

export enum EmployeeUpperEnum {
	ID = 'ID',
	NAME = 'NAME',
	PATERNAL_SURNAME = 'PATERNAL_SURNAME',
	MATERNAL_SURNAME = 'MATERNAL_SURNAME',
	EMAIL = 'EMAIL',
	PHONE = 'PHONE',
	CHARGE = 'CHARGE',
	AREA = 'AREA',
	SALARY = 'SALARY',
	CREATED_AT = 'CREATED_AT',
	UPDATED_AT = 'UPDATED_AT',
}

export interface IEmployeeWithId extends IEmployee {
	id: string;
}

export interface IEmployeesWithDate extends Omit<IEmployeeWithId, 'area' | 'charge'> {
	createdAt?: Date | string;
	updatedAt?: Date | string;
	charge: string;
	area: string;
}

export interface IEmployeeField {
	id: number;
	label: string;
	fieldName: string;
	type: string;
	placeholder: string;
	dataTestId: string;
	selectOptions?: ISelectOption[];
}

export interface ISelectOption {
	id: string;
	name: string;
}
