export enum AllowedAreasEnum {
	VentasMarketing = 'Ventas y Marketing',
	RecursosHumanos = 'Recursos Humanos',
	AdministracionFinanzas = 'Administración y Finanzas',
}
