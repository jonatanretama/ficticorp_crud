# Use the official Node.js 20 image alpine as a parent image
FROM node:20-alpine

# Install PNPM
RUN npm install -g pnpm

# Set the working directory in the container to /app
WORKDIR /app

# Copy the current directory contents into the container at /app
COPY . .

# Copy the package.json and pnpm-lock.yaml files into the container at /app
COPY package.json .
COPY pnpm-lock.yaml .

# Copy the .env.local file into the container at /app
COPY .env.local .env.local

# Install the dependencies
RUN pnpm install

# Build the application
RUN pnpm run build

# Make port 3000 available to the world outside this container
EXPOSE 3000

# Run the start command (production mode)
CMD ["pnpm", "start"]
