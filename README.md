This is a [Next.js](https://nextjs.org/) project bootstrapped with [`create-next-app`](https://github.com/vercel/next.js/tree/canary/packages/create-next-app).

## FictiCorp

Data management system enabling CRUD operations (Create, Read, Update, Delete) on the company's departments and job positions. The operations are on a CSV file in Google Drive using Google OAuth and Drive API.

See the result: [https://ficticorp-crud.vercel.app/](https://ficticorp-crud.vercel.app/)

**How does it work?**

1. First access, you will be redirected to auth page and then you should Sign in with Google OAuth.
2. You will be redirected to the Google flow; since the application is not verified, you will be shown a warning message. You should click on "Advanced settings" and then click on "Go to ficticorp-crud.vercel.app (unsafe)" to proceed with the flow.
3. Google will request authorization to access your information, and you must accept everything, including the section "View, edit, and delete all your Google Drive files."
4. Once you are signed in, you will be redirected to the home page, where you can view a table with all employees (if data exists); otherwise, you'll see a view to add a new employee.

In this section, you can add, edit, modify, or delete one or multiple employees.

- The CSV file is a shared file, making it visible to everyone.
- If you **add** or **edit** an employee, the **CSV file will be shared** with the email you used to log in. Therefore, once you make a modification, if you access your Google Drive, you should see the file employees.csv in the "Shared with me" section.
- In the top menu, you will find the option to log out.

_If you wish to revoke the permissions granted upon logging in, you can remove them from your Google account:_ [https://myaccount.google.com/connections](https://myaccount.google.com/connections)

## Run project

### Running in local environment

_The instructions for running with Docker can be found below._

1. Clone the project repository

   ```bash
   git clone https://gitlab.com/jonatanretama/ficticorp_crud.git
   ```

2. Access the project folder
   ```bash
   cd ficticorp_crud
   ```
3. Install dependencies, you can use npm, yarn or pnpm. In this case, I use pnpm.
   - `pnpm install` or `yarn install` or `npm install`
4. Create a `.env.local` file in the root of the project and add the following environment variables:
   ```bash
   NEXTAUTH_SECRET=NEXTAUTH_SECRET
   NEXT_PUBLIC_GOOGLE_CLIENT_ID=NEXT_PUBLIC_GOOGLE_CLIENT_ID
   NEXT_PUBLIC_GOOGLE_CLIENT_SECRET=NEXT_PUBLIC_GOOGLE_CLIENT_SECRET
   NEXT_PUBLIC_GOOGLE_DRIVE_BASE_URL=https://www.googleapis.com
   NEXT_PUBLIC_GOOGLE_DRIVE_FILE_ID=SHARED_FILE_ID
   NEXT_PUBLIC_OAUTH2_TOKEN_URL=https://oauth2.googleapis.com/token
   ```
   - Replace values with your own.
5. Build the project
   - `pnpm build` or `yarn build` or `npm run build`
6. Run the project (production)
   - `pnpm start` or `yarn start` or `npm start`
7. Open [http://localhost:3000](http://localhost:3000) with your browser to see the result.

- For run development mode, you can use the following command:
  - `pnpm dev` or `yarn dev` or `npm run dev`

### Running with Docker

**To run with Docker, you need to have Docker installed and/or Docker Desktop.**

Once the repository is cloned, you also need to have the environment variables declared in your `.env.local` file located at the root.

- In the root of the project, run the following command to build the image:
  ```bash
  docker build -t ficticorp-app .
  ```
- After the image is built, create a container with the following command:
  ```bash
  docker create -p 3000:3000 --name ficticorp-container ficticorp-app
  ```
- Start the container:
  ```bash
  docker start ficticorp-container
  ```
- Open [http://localhost:3000](http://localhost:3000) with your browser to see the result.
- To stop the container, run:
  ```bash
  docker stop ficticorp-container
  ```

## Learn More

This project is configured to work with the Google Drive API. To learn more about this, access: [https://developers.google.com/drive/api/guides/about-files](https://developers.google.com/drive/api/guides/about-files)

## Technologies

Next.js | TypeScript | Tailwind CSS | Google OAuth | Google Drive API | NextAuth.js | Axios | CSV parser (papaparse) | React Hook Form | Yup | TanStack Table | Headless UI | Heroicons | ESLint | Prettier | Husky | Lint-staged | Commitlint | Conventional Commits | sonner | Zustand | uuid

_Deployed on [Vercel Platform](https://vercel.com/new?utm_medium=default-template&filter=next.js&utm_source=create-next-app&utm_campaign=create-next-app-readme)_
