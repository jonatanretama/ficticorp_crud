module.exports = {
	'**/*.(ts|tsx|js|jsx)': filenames => [
		`eslint --fix ${filenames.join(' ')}`,
		`prettier --ignore-path .gitignore --ignore-path .prettierignore --write ${filenames.join(' ')}`,
	],

	'**/*.(md|json)': filenames => [
		`prettier --ignore-path .gitignore --ignore-path .prettierignore --write ${filenames.join(' ')}`,
	],
};
