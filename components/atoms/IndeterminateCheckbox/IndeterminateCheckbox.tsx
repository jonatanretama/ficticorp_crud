import { cn } from '@/utils/cn';
import { useRef, useEffect, HTMLProps } from 'react';

/**
 * @description Indeterminate checkbox component
 */
export const IndeterminateCheckbox = ({
	indeterminate,
	className = '',
	...rest
}: { indeterminate?: boolean } & HTMLProps<HTMLInputElement>) => {
	const ref = useRef<HTMLInputElement>(null!);

	useEffect(() => {
		if (typeof indeterminate === 'boolean') {
			ref.current.indeterminate = !rest.checked && indeterminate;
		}
	}, [ref, indeterminate]);

	return (
		<input
			type="checkbox"
			ref={ref}
			className={cn(
				className,
				'text-rose-500 focus:ring-rose-500 focus:border-transparent border-gray-300 cursor-pointer focus:outline-none focus:ring-1 checked:bg-rose-500 checked:border-transparent indeterminate:bg-rose-500 focus:active:checked:bg-rose-500 focus:hover:checked:bg-rose-500 hover:checked:bg-rose-600 active:checked:bg-rose-500 '
			)}
			{...rest}
		/>
	);
};
