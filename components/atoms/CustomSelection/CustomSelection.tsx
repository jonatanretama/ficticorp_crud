import { Fragment } from 'react';
import { Listbox, Transition } from '@headlessui/react';
import { CheckIcon, ChevronUpDownIcon } from '@heroicons/react/20/solid';
import { ISelectOption } from '@/models/IEmployee';
import { cn } from '@/utils/cn';

interface ICustomSelectionProps {
	/** OnChange function to set selected value */
	onChange: (value: ISelectOption) => void;
	/** Value of the select */
	value: ISelectOption;
	/** Options for the select */
	selectOptions: ISelectOption[];
	/** Data test id for testing */
	dataTestId?: string;
	/** Error state */
	error?: boolean;
}

/**
 * @description Custom selection component (Listbox)
 */
export const CustomSelection = ({ selectOptions, onChange, value, dataTestId, error }: ICustomSelectionProps) => {
	return (
		<div className="w-full">
			<Listbox value={value} onChange={onChange}>
				<div className="relative">
					<Listbox.Button
						data-testid={dataTestId}
						className={cn(
							'text-gray-600 relative w-full h-9 cursor-pointer rounded-lg focus:ring-2 focus:ring-rose-400 bg-white border-[1px] focus:border-transparent ring-none mt-0 py-0 pl-3 pr-10 text-left focus:outline-none text-md',
							error && 'border-red-500'
						)}>
						<span className="block truncate">{value.name}</span>
						<span className="pointer-events-none absolute inset-y-0 right-0 flex items-center pr-2">
							<ChevronUpDownIcon className="h-5 w-5 text-gray-400" aria-hidden="true" />
						</span>
					</Listbox.Button>
					<Transition as={Fragment} leave="transition ease-in duration-100" leaveFrom="opacity-100" leaveTo="opacity-0">
						<Listbox.Options className="absolute mt-1 max-h-60 w-full overflow-auto rounded-md bg-white py-1 text-xs shadow-xl ring-1 ring-black/5 focus:outline-none sm:text-sm z-50">
							<Listbox.Option
								value=""
								key="default-option"
								data-testid={`${dataTestId}_DEFAULT`}
								disabled
								className="relative cursor-default select-none py-2 pl-10 pr-4 text-gray-400">
								<span className={`block truncate ${value ? 'font-medium' : 'font-normal'}`}>Selecciona</span>
							</Listbox.Option>
							{selectOptions.map((option: ISelectOption) => (
								<Listbox.Option
									key={`${option.id}-${option.name}`}
									data-testid={`${dataTestId}_${selectOptions.find(item => item.id === option.id)?.id.toUpperCase()}`}
									className={({ active }) =>
										`relative cursor-pointer select-none py-2 pl-10 pr-4 ${
											active ? 'bg-rose-100 text-rose-900' : 'text-gray-900'
										}`
									}
									value={option}>
									{() => {
										const isSelected = value.id === option.id;
										return (
											<>
												<span className={`block truncate ${isSelected ? 'font-medium' : 'font-normal'}`}>
													{option.name}
												</span>
												{isSelected ? (
													<span className="absolute inset-y-0 left-0 flex items-center pl-3 text-rose-600">
														<CheckIcon className="h-5 w-5" aria-hidden="true" />
													</span>
												) : null}
											</>
										);
									}}
								</Listbox.Option>
							))}
						</Listbox.Options>
					</Transition>
				</div>
			</Listbox>
		</div>
	);
};
