import { cn } from '@/utils/cn';

interface IButtonBasic {
	/** Content in button */
	children: React.ReactNode;
	/** Function to run when button is clicked */
	onClick?: () => void;
	/** Classname for button */
	className?: HTMLButtonElement['className'];
	/** Type of button */
	type?: 'button' | 'submit' | 'reset';
}

/**
 * @description Basic button component
 */
export const ButtonBasic = ({ onClick, children, className, type = 'button' }: IButtonBasic) => (
	<button
		type={type}
		className={cn(
			'text-sm border rounded-lg px-2 py-1 flex flex-nowrap gap-1 justify-center items-center focus:ring-rose-500 focus:ring-1 focus:border-transparent focus:outline-none active:ring-rose-500 active:ring-1',
			className
		)}
		onClick={onClick}>
		{children}
	</button>
);
