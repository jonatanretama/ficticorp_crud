import { styleErrorSpan, styleInput } from '@/common/tailwindStyle';
import { IEmployeeField } from '@/models/IEmployee';
import { Controller } from 'react-hook-form';
import { NumericFormat } from 'react-number-format';
interface IInputNumberCustomProps {
	/** Control from react hook form */
	control: any;
	/** Register from react hook form */
	register?: any;
	/** Errors from react hook form */
	errors: any;
	/** Field data */
	fieldData: IEmployeeField;
}

/**
 * @description Input number controlled component
 * If currency type is passed, it will add a $ prefix and a decimal scale of 2 with "," as thousand separator
 */
export const InputNumberControlled = ({ control, errors, fieldData }: IInputNumberCustomProps) => {
	const { fieldName, type, placeholder, dataTestId } = fieldData;

	const styleError = errors[fieldName] && 'border-red-500 border-[1px]';

	return (
		<Controller
			name={fieldName}
			control={control}
			render={({ field: { onBlur, onChange, value } }) => (
				<div className="flex flex-col">
					<NumericFormat
						onChange={onChange}
						onBlur={onBlur}
						value={value}
						thousandSeparator=","
						{...(type === 'currency' && { prefix: '$', decimalScale: 2, min: 0, allowNegative: false })}
						className={styleInput(styleError)}
						id={fieldName}
						placeholder={placeholder}
						datatest-id={dataTestId}
						autoComplete={type === 'currency' ? 'transaction-amount' : 'on'}
					/>
					{!!errors[fieldName] && <span className={styleErrorSpan()}>{errors[fieldName]?.message}</span>}
				</div>
			)}
		/>
	);
};
