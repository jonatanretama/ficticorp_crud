import { Controller } from 'react-hook-form';
import { CustomSelection } from '../CustomSelection/CustomSelection';
import { ISelectOption } from '@/models/IEmployee';

interface ICustomSelectionProps {
	/** Control from react hook form */
	control: any;
	/** Field name */
	fieldName: string;
	/** Options for the select */
	selectOptions: ISelectOption[];
	/** Errors state from react hook form */
	errors: any;
	/** Data test id for testing */
	dataTestId: string;
}

/**
 * @description Select controlled component (Listbox)
 */
export const SelectControlled = ({ control, fieldName, selectOptions, errors, dataTestId }: ICustomSelectionProps) => {
	return (
		<Controller
			name={fieldName}
			control={control}
			render={({ field: { onChange, value } }) => (
				<div>
					<CustomSelection
						dataTestId={dataTestId}
						selectOptions={selectOptions}
						onChange={onChange}
						value={value}
						error={!!errors[fieldName]}
					/>
					{!!errors[fieldName] && (
						<span className="text-red-500 text-xs italic">{errors[fieldName]?.name?.message}</span>
					)}
				</div>
			)}
		/>
	);
};
