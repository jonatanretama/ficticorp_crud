import { cn } from '@/utils/cn';

interface ISpinnerProps {
	/** ClassName for the container */
	classNameContainer?: string;
	/** ClassName for the spinner (svg) */
	classNameSpinner?: string;
}

/**
 * @description Spinner component for loading
 */
export const Spinner = ({ classNameContainer, classNameSpinner }: ISpinnerProps) => {
	return (
		<div
			className={cn(
				'fixed top-0 left-0 w-full h-full bg-gray-900 backdrop-blur-sm bg-opacity-40 flex items-center justify-center z-50',
				classNameContainer
			)}>
			<svg
				className={cn('animate-spin h-14 w-14 text-white', classNameSpinner)}
				xmlns="http://www.w3.org/2000/svg"
				fill="none"
				viewBox="0 0 24 24">
				<circle className="opacity-25" cx="12" cy="12" r="10" stroke="currentColor" strokeWidth="4"></circle>
				<path
					className="opacity-75"
					fill="currentColor"
					d="M4 12a8 8 0 018-8V0C5.373 0 0 5.373 0 12h4zm2 5.291A7.96 7.96 0 014 12H0c0 3.042 1.135 5.824 3 7.938l3-2.647zM20 12c0-3.042-1.135-5.824-3-7.938l-3 2.647A7.96 7.96 0 0116 12h4z"></path>
			</svg>
		</div>
	);
};
