import { styleErrorSpan, styleInput } from '@/common/tailwindStyle';
import { IEmployeeField } from '@/models/IEmployee';
import { Controller } from 'react-hook-form';

interface IInputTextCustomProps {
	/** Control from react hook form */
	control: any;
	/** Register from react hook form */
	register?: any;
	/** Errors from react hook form */
	errors: any;
	/** Field data */
	fieldData: IEmployeeField;
}

/**
 * @description Input text controlled component
 */
export const InputTextCustom = ({ control, register, errors, fieldData }: IInputTextCustomProps) => {
	const { fieldName, type, placeholder, dataTestId } = fieldData;
	const styleError = errors[fieldName] && 'border-red-500';
	return (
		<Controller
			name={fieldName}
			control={control}
			render={({ field }) => (
				<>
					<input
						{...field}
						{...register(fieldName)}
						className={styleInput(styleError)}
						id={fieldName}
						type={type}
						placeholder={placeholder}
						data-testid={dataTestId}
						autoComplete="on"
					/>
					{!!errors[fieldName] && <span className={styleErrorSpan()}>{errors[fieldName]?.message}</span>}
				</>
			)}
		/>
	);
};
