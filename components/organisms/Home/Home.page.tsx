import DialogEmployee from '@/components/molecules/DialogEmployee/DialogEmployee';
import TableEmployees from '@/components/molecules/TableEmployees/TableEmployees';

/**
 * @description Home page with the table of employees and the dialog to create or edit an employee
 */
const Home = () => {
	return (
		<>
			<DialogEmployee />
			<section aria-label="table" className="mt-10">
				<TableEmployees />
			</section>
		</>
	);
};

export default Home;
