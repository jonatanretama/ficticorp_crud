'use client';

import React, { useEffect } from 'react';
import { motion } from 'framer-motion';
import { LampContainer } from '@/components/molecules/LampContainer/LampContainer';
import { signIn, useSession } from 'next-auth/react';
import Image from 'next/image';
import googleSvg from '../../../public/icons/google.svg';
import { useRouter } from 'next/navigation';

/**
 * @description Login page using next-auth
 */
const Login = () => {
	const router = useRouter();
	const { data: session } = useSession();

	useEffect(() => {
		if (session?.accessToken) {
			router.push('/');
		}
	}, [session?.accessToken]);

	return (
		<LampContainer>
			<motion.h1
				initial={{ opacity: 0, y: 100 }}
				whileInView={{ opacity: 1, y: -100 }}
				transition={{
					delay: 0.3,
					duration: 0.8,
					ease: 'easeInOut',
				}}
				className="w-full mt-8 bg-gradient-to-br from-slate-300 to-slate-500 py-4 bg-clip-text text-center text-4xl font-medium tracking-tight text-transparent md:text-7xl">
				<div className="w-full justify-center items-center flex flex-col gap-4">
					<button
						type="button"
						onClick={() => signIn('google')}
						className="flex flex-nowrap justify-center items-center border border-white rounded-xl focus:bg-white text-white bg-transparent hover:bg-white hover:text-black px-4 py-2 font-medium tracking-tight text-lg md:text-2xl">
						<Image
							src={googleSvg}
							alt="Google"
							className="mix-blend-difference w-6 h-6 mr-2 text-red-400"
							width={48}
							height={48}
						/>
						Inicia sesión con Google
					</button>
				</div>
			</motion.h1>
		</LampContainer>
	);
};

export default Login;
