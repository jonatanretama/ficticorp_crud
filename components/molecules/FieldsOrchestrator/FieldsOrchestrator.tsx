import { InputNumberControlled } from '@/components/atoms/InputNumberControlled/InputNumberControlled';
import { InputTextCustom } from '@/components/atoms/InputTextCustom/InputTextCustom';
import { SelectControlled } from '@/components/atoms/SelectControlled/SelectControlled';
import { IEmployee, IEmployeeField, ISelectOption } from '@/models/IEmployee';
import { cn } from '@/utils/cn';
import { HTMLAttributes } from 'react';
import { Control, FieldErrors, UseFormRegister } from 'react-hook-form';

interface IFieldsOrchestratorProps {
	/** Fields to display */
	fields: IEmployeeField[];
	/** Errors from react hook form */
	errors: FieldErrors<IEmployee>;
	/** Register from react hook form */
	register: UseFormRegister<IEmployee>;
	/** Control from react hook form */
	control: Control<IEmployee>;
	/** Classname for the container */
	className?: HTMLAttributes<HTMLButtonElement>['className'];
}

/**
 * @description Orchestrator for fields
 */
export const FieldsOrchestrator = ({ errors, register, control, className, fields }: IFieldsOrchestratorProps) => {
	return (
		<div className={cn('grid grid-cols-1 sm:grid-cols-2 lg:grid-cols-3 gap-8 w-full', className)}>
			{fields.map(field => {
				// If field provided is a select, return a select component
				if (field.type === 'select') {
					return (
						<div key={`${field.fieldName}-${field.id}`}>
							<label className="block text-gray-700 text-sm font-bold mb-1">{field.label}</label>
							<SelectControlled
								fieldName={field.fieldName}
								selectOptions={field.selectOptions as ISelectOption[]}
								control={control}
								errors={errors}
								dataTestId={field.dataTestId}
							/>
						</div>
					);
				}

				// If field provided is a currency or number, return a input number component
				if (['currency', 'number'].includes(field.type)) {
					return (
						<div key={`${field.fieldName}-${field.id}`}>
							<label htmlFor={field.fieldName} className="block text-gray-700 text-sm font-bold mb-1">
								{field.label}
							</label>
							<InputNumberControlled control={control} register={register} errors={errors} fieldData={field} />
						</div>
					);
				}

				// Else, return a input text component
				return (
					<div key={`${field.fieldName}-${field.id}`}>
						<label htmlFor={field.fieldName} className="block text-gray-700 text-sm font-bold mb-1">
							{field.label}
						</label>
						<InputTextCustom control={control} register={register} errors={errors} fieldData={field} />
					</div>
				);
			})}
		</div>
	);
};
