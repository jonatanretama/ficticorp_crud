'use client';

import TableCustom from '../TableCustom/TableCustom';
import { useEmployeeStore } from '@/store/employeeStore';
import { UserPlusIcon } from '@heroicons/react/24/outline';
import { ButtonBasic } from '@/components/atoms/ButtonBasic/ButtonBasic';
import { useTableEmployees } from '@/hooks/useTableEmployees';
import { Spinner } from '@/components/atoms/Spinner/Spinner';

/**
 * @description Table employees component using react-table of tanstack
 */
const TableEmployees = () => {
	const { allEmployees, setDialogOpen, isEmployeesFetched, isFetching } = useEmployeeStore();
	const { columns, handleDeleteEmployeeBulk } = useTableEmployees();

	const Button = () => (
		<ButtonBasic onClick={() => setDialogOpen(true)} className="h-9">
			<UserPlusIcon className="h-4 w-4" />
			Nuevo
		</ButtonBasic>
	);

	// Display a button and a chip with the text "Procesando ..." when isFetching is true in front of the title of the table
	const ContentInFrontTitle = () => (
		<div className="flex flex-nowrap gap-4 items-center justify-center">
			<Button />
			{isFetching && (
				<span className="bg-green-100 rounded-xl border border-green-500 px-2 py-0 h-7 text-sm font-light italic flex justify-center items-center">
					Procesando ...
				</span>
			)}
		</div>
	);

	// Display a spinner in all screen when isFetching is true and isEmployeesFetched is false
	if (!isEmployeesFetched && isFetching) {
		return (
			<div className="flex items-center justify-center flex-col min-h-dvh -mt-20 gap-6 text-center relative">
				<Spinner />
			</div>
		);
	}

	// Display a message when there are no employees
	if (allEmployees.length === 0 && !isFetching) {
		return (
			<div className="flex items-center justify-center flex-col min-h-dvh -mt-20 gap-6 text-center">
				<h1 className="text-4xl font-bold drop-shadow-md text-gray-600">No hay empleados, prueba agregando uno</h1>
				<Button />
			</div>
		);
	}

	return (
		<TableCustom
			data={allEmployees as unknown as Record<string, unknown>[]}
			columns={columns}
			title="Empleados"
			contentInFrontTitle={<ContentInFrontTitle />}
			handleDeleteEmployeeBulk={handleDeleteEmployeeBulk}
		/>
	);
};

export default TableEmployees;
