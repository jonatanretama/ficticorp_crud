import { IndeterminateCheckbox } from '@/components/atoms/IndeterminateCheckbox/IndeterminateCheckbox';
import { PencilSquareIcon, TrashIcon } from '@heroicons/react/24/solid';
import dayjs from 'dayjs';
import { IEmployeesWithDate, IEmployeeWithId } from '../../../models/IEmployee';
import { Row, Table } from '@tanstack/react-table';
import { tableEmployeeToFormAdapter } from '@/adapters/tableEmployeeToFormAdapter';

interface IColumnsTableEmployees {
	/** Function to open modal */
	handleOpenModal: () => void;
	/** Function to edit employee action */
	editEmployee: (employee: IEmployeeWithId) => void;
	/** Function to delete one employee */
	handleDeleteOneEmployee: (employee: IEmployeesWithDate) => void;
}

/**
 * @description Columns for the employees table using react table of tansctack
 */
export const columnsTableEmployees = ({
	handleOpenModal,
	editEmployee,
	handleDeleteOneEmployee,
}: IColumnsTableEmployees) => {
	const handleEditEmployee = (rowData: IEmployeeWithId) => {
		const adaptedData = tableEmployeeToFormAdapter(rowData);
		editEmployee(adaptedData);
		handleOpenModal();
	};

	return [
		{
			id: 'select',
			header: ({ table }: { table: Table<unknown> }) => (
				<IndeterminateCheckbox
					{...{
						checked: table.getIsAllRowsSelected(),
						indeterminate: table.getIsSomeRowsSelected(),
						onChange: table.getToggleAllRowsSelectedHandler(),
					}}
				/>
			),
			cell: ({ row }: { row: Row<IEmployeeWithId> }) => (
				<div className="px-1">
					<IndeterminateCheckbox
						{...{
							checked: row.getIsSelected(),
							disabled: !row.getCanSelect(),
							indeterminate: row.getIsSomeSelected(),
							onChange: row.getToggleSelectedHandler(),
						}}
					/>
				</div>
			),
		},
		{
			header: 'ID',
			accessorKey: 'id',
			cell: ({ getValue }: { getValue: () => string }) => <div className="truncate w-20">{getValue()}</div>,
		},
		{
			header: 'Nombre',
			accessorKey: 'name',
		},
		{
			header: 'A. Paterno',
			accessorKey: 'paternalSurname',
		},
		{
			header: 'A. Materno',
			accessorKey: 'maternalSurname',
		},
		{
			header: 'Correo Electrónico',
			accessorKey: 'email',
		},
		{
			header: 'Télefono',
			accessorKey: 'phone',
		},
		{
			header: 'Puesto',
			accessorKey: 'charge',
		},
		{
			header: 'Área',
			accessorKey: 'area',
		},
		{
			header: 'Sueldo',
			accessorKey: 'salary',
		},
		{
			header: 'Fecha creación',
			accessorKey: 'createdAt',
			cell: ({ getValue }: { getValue: () => string }) => (
				<div className="flex flex-wrap whitespace-normal">
					{getValue() && dayjs(getValue()).format('DD/MM/YYYY HH:mm')}
				</div>
			),
		},
		{
			header: 'Ultima modificación',
			accessorKey: 'updatedAt',
			cell: ({ getValue }: { getValue: () => string }) => (
				<div className="flex flex-wrap whitespace-normal">
					{getValue() && dayjs(getValue()).format('DD/MM/YYYY HH:mm')}
				</div>
			),
		},

		{
			header: 'Acciones',
			cell: ({ row }: { row: Row<IEmployeeWithId> }) => (
				<div className="text-left flex flex-nowrap gap-2">
					<button
						className="rounded-lg shadow-lg bg-blue-500/20 active:bg-blue-500/30 p-2 px-2.5 flex flex-nowrap gap-1 items-center justify-center"
						onClick={() => handleEditEmployee(row.original)}>
						<PencilSquareIcon className="w-4 h-4 text-blue-700" />
					</button>
					<button
						className="rounded-lg shadow-lg bg-red-500/20 active:bg-red-500/30 p-2 px-2.5 flex flex-nowrap gap-1 items-center justify-center"
						onClick={() => handleDeleteOneEmployee(row.original as unknown as IEmployeesWithDate)}>
						<TrashIcon className="w-4 h-4 text-red-700" />
					</button>
				</div>
			),
		},
	];
};
