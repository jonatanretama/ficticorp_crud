'use client';

import { DialogCustom } from '@/components/atoms/DialogCustom/DialogCustom';
import { FormEmployee } from '../FormEmployee/FormEmployee';
import { useEmployeeStore } from '@/store/employeeStore';
import { defaultEmployee } from '@/data/defaultEmployee';
import { Spinner } from '@/components/atoms/Spinner/Spinner';

/**
 * @description Dialog employee component using the DialogCustom component
 */
const DialogEmployee = () => {
	const { employee, isDialogOpen, isFetching } = useEmployeeStore();
	const { editEmployee, setDialogOpen } = useEmployeeStore();

	const handleOnClose = () => {
		setDialogOpen(false);

		// Await for the dialog to close before updating the employee to avoid flickering
		setTimeout(() => editEmployee(defaultEmployee), 200);
	};

	const isUpdate = !!employee.id;

	return (
		<DialogCustom
			isOpen={isDialogOpen}
			onClose={() => handleOnClose()}
			title={isUpdate ? 'Actualizar empleado' : 'Agregar empleado'}>
			<div className="flex flex-col w-full" data-testid="DIALOG_EMPLOYEE">
				{isFetching && <Spinner />}
				<FormEmployee />
			</div>
		</DialogCustom>
	);
};

export default DialogEmployee;
