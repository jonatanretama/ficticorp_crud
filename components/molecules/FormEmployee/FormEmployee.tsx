'use client';

import { SubmitHandler, useForm } from 'react-hook-form';
import { yupResolver } from '@hookform/resolvers/yup';
import { IEmployee, IEmployeesWithDate } from '@/models/IEmployee';
import { configEmployeeSchema } from './employeeSchema';
import { FieldsOrchestrator } from '../FieldsOrchestrator/FieldsOrchestrator';
import { employeeFields } from './employeeFields';
import { useEmployeeStore } from '@/store/employeeStore';
import { employeeToPostAdapter } from '@/adapters/employeeToPostAdapter';
import { toast } from 'sonner';
import { useSession } from 'next-auth/react';
import { getFilePermissions } from '@/services/drive-services';
import { patchCSVFile } from '@/services/employee-services';
import { employeeToPatchAdapter } from '@/adapters/employeeToPatchAdapter';
import { useGoogleDriveAPI } from '@/hooks/useDriveInstance';
import { defaultEmployee } from '@/data/defaultEmployee';
import { ButtonBasic } from '@/components/atoms/ButtonBasic/ButtonBasic';

/**
 * @description Form employee component validated with yup and using react-hook-form
 */
export const FormEmployee = () => {
	const { data: session } = useSession();
	const driveApi = useGoogleDriveAPI();
	const { employee, setFetching, editEmployee, postEmployee, setDialogOpen, allEmployees, updateEmployee } =
		useEmployeeStore();

	const {
		register,
		handleSubmit,
		control,
		formState: { errors },
	} = useForm<IEmployee>({
		resolver: yupResolver<IEmployee>(configEmployeeSchema),
		defaultValues: {
			name: employee.name,
			paternalSurname: employee.paternalSurname,
			maternalSurname: employee.maternalSurname,
			email: employee.email,
			phone: employee.phone,
			charge: employee.charge,
			area: employee.area,
			salary: employee.salary,
		},
	});

	const handleCancel = () => {
		setDialogOpen(false);
		setTimeout(() => editEmployee(defaultEmployee), 200);
	};

	const onSubmit: SubmitHandler<IEmployee> = async data => {
		let adaptedData = {};
		let dataToSave = [];

		if (employee.id) {
			adaptedData = employeeToPatchAdapter(data, employee as unknown as IEmployeesWithDate);
			dataToSave = allEmployees.map(e => (e.id === employee.id ? adaptedData : e));
		} else {
			adaptedData = employeeToPostAdapter(data);
			dataToSave = [adaptedData, ...allEmployees];
		}
		setFetching(true);

		// Set the permissions for the file according to the user's email and share the file
		getFilePermissions({
			email: session?.user?.email as string,
			driveApi,
		});

		// Patch the CSV file with the new data into the Google Drive
		await patchCSVFile({ data: dataToSave, driveApi })
			.then(response => {
				if (response?.name) {
					toast.success(`Empleado ${employee.id ? 'actualizado' : 'creado'}`, {
						description: `${data.name} ${data.paternalSurname} ${data.maternalSurname}`,
					});

					if (employee.id) {
						updateEmployee(adaptedData as IEmployeesWithDate);
						setTimeout(() => editEmployee(defaultEmployee), 200);
					} else {
						postEmployee(adaptedData as IEmployeesWithDate);
						setTimeout(() => editEmployee(defaultEmployee), 200);
					}
				}
			})
			.catch(error => {
				toast.error(`Error al ${employee.id ? 'actualizar' : 'crear'} empleado`, { description: error });
			})
			.then(() => {
				setFetching(false);
			});

		setDialogOpen(false);
	};

	return (
		<form onSubmit={handleSubmit(onSubmit)} className="w-full" datatest-id="EMPLOYEE_FORM">
			<FieldsOrchestrator control={control} register={register} errors={errors} fields={employeeFields} />

			<div className="w-full flex justify-center items-center gap-2">
				<ButtonBasic
					className="mt-10 h-12 w-auto border-gray-400 border-[2px] px-4"
					type="button"
					onClick={handleCancel}>
					Cancelar
				</ButtonBasic>
				<ButtonBasic className="mt-10 h-12 w-auto border-gray-500 border-[2px] px-4" type="submit">
					{employee.id ? 'Actualizar' : 'Añadir'}
				</ButtonBasic>
			</div>
		</form>
	);
};
