import { AllowedAreasEnum } from '@/models/AllowedAreasEnum';
import { AllowedChargesEnum } from '@/models/AllowedChargesEnum';
import * as yup from 'yup';

/**
 * @description Schema for the employee form, is a regular expression for phone numbers
 */
const phoneRegExp = /^\d{7,15}$/;

/**
 * @description Schema for the employee form using yup
 */
export const configEmployeeSchema = yup.object({
	name: yup.string().required('Nombre es requerido.'),
	paternalSurname: yup.string().required('Apellido Paterno es requerido.'),
	maternalSurname: yup.string().required('Apellido Materno es requerido.'),
	email: yup.string().required('Correo electrónico es requerido.'),
	phone: yup
		.string()
		.required('Teléfono del ganador es requerido.')
		.matches(phoneRegExp, 'Número de teléfono no válido.'),
	charge: yup.object().shape({
		id: yup.string().required('ID es requerido.'),
		name: yup
			.string()
			.oneOf(Object.values(AllowedChargesEnum), 'Selecciona un Puesto.')
			.required('Puesto es requerido.'),
	}),
	area: yup.object().shape({
		id: yup.string().required('ID es requerido.'),
		name: yup.string().oneOf(Object.values(AllowedAreasEnum), 'Selecciona una Área.').required('Área es requerida.'),
	}),
	salary: yup.string().required('Sueldo es requerido.'),
	createdAt: yup.string(),
	updatedAt: yup.string(),
	string: yup.string(),
});
