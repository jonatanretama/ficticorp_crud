import { areasOptions, chargesOptions } from '@/data/selectOptions';
import { EmployeeUpperEnum, IEmployeeField } from '@/models/IEmployee';

/**
 * @description Fields for the employee form
 * The fields are mapped in the FormEmployee component by the FieldsOrchestrator component
 */
export const employeeFields: IEmployeeField[] = [
	{
		id: 1,
		label: 'Área',
		fieldName: 'area',
		type: 'select',
		placeholder: 'Área',
		dataTestId: EmployeeUpperEnum.AREA,
		selectOptions: areasOptions,
	},
	{
		id: 2,
		label: 'Puesto',
		fieldName: 'charge',
		type: 'select',
		placeholder: 'Puesto',
		dataTestId: EmployeeUpperEnum.CHARGE,
		selectOptions: chargesOptions,
	},
	{
		id: 3,
		label: 'Sueldo',
		fieldName: 'salary',
		type: 'currency',
		placeholder: 'Ej. $10,000.00',
		dataTestId: EmployeeUpperEnum.SALARY,
	},
	{
		id: 4,
		label: 'Nombre',
		fieldName: 'name',
		type: 'text',
		placeholder: 'Escribe tu nombre',
		dataTestId: EmployeeUpperEnum.NAME,
	},
	{
		id: 5,
		label: 'Apellido Paterno',
		fieldName: 'paternalSurname',
		type: 'text',
		placeholder: 'Escribe tu apellido paterno',
		dataTestId: EmployeeUpperEnum.PATERNAL_SURNAME,
	},
	{
		id: 6,
		label: 'Apellido Materno',
		fieldName: 'maternalSurname',
		type: 'text',
		placeholder: 'Escribe tu apellido materno',
		dataTestId: EmployeeUpperEnum.MATERNAL_SURNAME,
	},
	{
		id: 7,
		label: 'Correo Electrónico',
		fieldName: 'email',
		type: 'email',
		placeholder: 'Ej. nombre@gmail.com',
		dataTestId: EmployeeUpperEnum.EMAIL,
	},
	{
		id: 8,
		label: 'Teléfono',
		fieldName: 'phone',
		type: 'tel',
		placeholder: 'Escribe tu teléfono',
		dataTestId: EmployeeUpperEnum.PHONE,
	},
];
