'use client';

import { Menu, Transition } from '@headlessui/react';
import { Fragment } from 'react';
import { ChevronDownIcon } from '@heroicons/react/20/solid';
import { ArrowLeftStartOnRectangleIcon } from '@heroicons/react/24/outline';
import { signOut, useSession } from 'next-auth/react';
import Image from 'next/image';

/**
 * @description Menu dropdown component (use next-auth)
 */
export const MenuDropdown = () => {
	const { data: session } = useSession();
	const imageUsr = session?.user?.image;
	const nameUsr = session?.user?.name;

	return (
		<div className="text-right">
			<Menu as="div" className="relative inline-block text-left">
				<Menu.Button className="inline-flex w-full justify-center items-center rounded-md bg-transparent px-4 py-2 text-sm font-medium text-black hover:bg-black/5 focus:outline-none focus-visible:ring-2 focus-visible:ring-white/75">
					{imageUsr && (
						<Image className="h-8 w-8 rounded-full mr-2" src={imageUsr} alt="Foto de perfil" width={32} height={32} />
					)}
					{nameUsr}
					<ChevronDownIcon className="-mr-1 ml-2 h-5 w-5 text-violet-200 hover:text-violet-100" aria-hidden="true" />
				</Menu.Button>
				<Transition
					as={Fragment}
					enter="transition ease-out duration-100"
					enterFrom="transform opacity-0 scale-95"
					enterTo="transform opacity-100 scale-100"
					leave="transition ease-in duration-75"
					leaveFrom="transform opacity-100 scale-100"
					leaveTo="transform opacity-0 scale-95">
					<Menu.Items className="absolute right-0 mt-2 w-56 origin-top-right divide-y divide-gray-100 rounded-md bg-white shadow-lg ring-1 ring-black/5 focus:outline-none">
						<div className="px-1 py-1 ">
							<Menu.Item>
								{({ active }) => (
									<button
										onClick={() => signOut()}
										className={`${
											active ? 'bg-rose-500 text-white' : 'text-gray-900'
										} group flex w-full items-center rounded-md px-2 py-2 text-sm`}>
										<ArrowLeftStartOnRectangleIcon className="w-5 h-5 mr-2" aria-hidden="true" />
										Cerrar sesión
									</button>
								)}
							</Menu.Item>
						</div>
					</Menu.Items>
				</Transition>
			</Menu>
		</div>
	);
};
