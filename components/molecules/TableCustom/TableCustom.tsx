'use client';

import { ButtonBasic } from '@/components/atoms/ButtonBasic/ButtonBasic';
import { IndeterminateCheckbox } from '@/components/atoms/IndeterminateCheckbox/IndeterminateCheckbox';
import { cn } from '@/utils/cn';
import {
	ChevronDoubleLeftIcon,
	ChevronDoubleRightIcon,
	ChevronLeftIcon,
	ChevronRightIcon,
} from '@heroicons/react/24/outline';
import { ArrowLongUpIcon, ArrowLongDownIcon } from '@heroicons/react/24/solid';
import {
	flexRender,
	useReactTable,
	getCoreRowModel,
	getPaginationRowModel,
	getSortedRowModel,
	getFilteredRowModel,
	ColumnDef,
	SortDirection,
	Row,
} from '@tanstack/react-table';
import { useState } from 'react';

interface ITableVerifierProps<T> {
	/** Data for the table */
	data: Record<string, unknown>[];
	/** Columns structure for React Table of Tanstack */
	// eslint-disable-next-line @typescript-eslint/no-explicit-any
	columns: ColumnDef<unknown, any>[];
	/** If the table has GlobalFilter */
	withGlobalFilter?: boolean;
	/** Title of the table */
	title: string;
	/** Content to display in front of title */
	contentInFrontTitle?: React.ReactNode;
	/** Number of rows to show per page */
	pageSize?: number;
	/** Default sorting */
	defaultSort?: [{ id: string; desc: boolean }] | [];
	/** Function to set the row selection */
	handleDeleteEmployeeBulk?: (data: Row<T>[]) => void;
}

/**
 * @description This component is a table that uses the React Table library from Tanstack.
 * @param data Data for the table
 * @param columns Columns structure for React Table of Tanstack
 * This renders a table with sorting, filtering and pagination.
 */
const TableCustom = <T,>({
	data,
	columns,
	withGlobalFilter = true,
	title,
	contentInFrontTitle,
	pageSize = 10,
	defaultSort = [],
	handleDeleteEmployeeBulk,
}: ITableVerifierProps<T>) => {
	const [sorting, setSorting] = useState(defaultSort);
	const [filtering, setFiltering] = useState('');
	const [pagination, setPagination] = useState({ pageIndex: 0, pageSize });

	const [rowSelection, setRowSelection] = useState({});

	const tableTanstack = useReactTable({
		data,
		columns,
		getCoreRowModel: getCoreRowModel(),
		getPaginationRowModel: getPaginationRowModel(),
		getSortedRowModel: getSortedRowModel(),
		getFilteredRowModel: getFilteredRowModel(),
		state: {
			sorting,
			globalFilter: filtering,
			pagination: pagination,
			rowSelection,
		},
		enableRowSelection: true,
		onRowSelectionChange: setRowSelection,
		onSortingChange: value => setSorting(value as any),
		onGlobalFilterChange: setFiltering,
		onPaginationChange: setPagination,
	});

	const SortedIcon = ({ order }: { order: SortDirection }) => {
		if (order === 'asc') return <ArrowLongUpIcon className="w-4 h-4 text-rose-500" />;
		if (order === 'desc') return <ArrowLongDownIcon className="w-4 h-4 text-rose-500" />;
		return null;
	};

	const handleChecksDelete = (data: Row<T>[]) => {
		if (handleDeleteEmployeeBulk) {
			handleDeleteEmployeeBulk(data);
		}
		setRowSelection({});
	};

	const selectedRowsLength = Object.keys(rowSelection).length;
	const totalRows = tableTanstack.getFilteredRowModel().rows.length;

	const pageBtnStyle = 'px-2 py-1 hover:cursor-pointer shadow-md active:bg-gray-300 flex rounded-lg';

	return (
		<section className="antialiased text-gray-700 items-center">
			<div className="flex flex-col justify-center h-full">
				<div className="w-full max-h-full mx-auto">
					<header className="px-5 py-4 border-y border-gray-100 mb-1">
						<div className="flex gap-2 lg:gap-4 flex-wrap items-center">
							<h6 className="block antialiased tracking-normal font-sans text-base font-semibold leading-relaxed text-gray-700 mb-0 drop-shadow-md">
								{title}
							</h6>
							{contentInFrontTitle && contentInFrontTitle}
							{withGlobalFilter && (
								<div className="relative w-full min-w-[200px] h-10 md:ml-auto md:w-auto">
									<input
										className="focus:ring-0 peer h-full w-full rounded-md border border-gray-200 bg-transparent px-3 py-2.5 font-sans text-sm font-normal text-gray-700 outline outline-0 transition-all placeholder-shown:border placeholder-shown:border-gray-200 placeholder-shown:border-t-gray-200 focus:border-2 focus:border-rose-500 focus:border-t-transparent border-t-transparent focus:outline-0 disabled:border-0 disabled:bg-gray-50"
										onChange={e => setFiltering(e.target.value)}
										value={filtering}
										placeholder=" "
									/>
									<label className="before:content[' '] after:content[' '] pointer-events-none absolute left-0 -top-1.5 flex h-full w-full select-none text-[11px] font-normal leading-tight text-gray-400 transition-all before:pointer-events-none before:mt-[6.5px] before:mr-1 before:box-border before:block before:h-1.5 before:w-2.5 before:rounded-tl-md before:border-t before:border-l before:border-gray-200 before:transition-all after:pointer-events-none after:mt-[6.5px] after:ml-1 after:box-border after:block after:h-1.5 after:w-2.5 after:flex-grow after:rounded-tr-md after:border-t after:border-r after:border-gray-200 after:transition-all peer-placeholder-shown:text-sm peer-placeholder-shown:leading-[3.75] peer-placeholder-shown:text-gray-500 peer-placeholder-shown:before:border-transparent peer-placeholder-shown:after:border-transparent peer-focus:text-[11px] peer-focus:leading-tight peer-focus:text-rose-500 peer-focus:before:border-t-2 peer-focus:before:border-l-2 peer-focus:before:border-rose-500 peer-focus:after:border-t-2 peer-focus:after:border-r-2 peer-focus:after:border-rose-500 peer-disabled:text-transparent peer-disabled:before:border-transparent peer-disabled:after:border-transparent peer-disabled:peer-placeholder-shown:text-gray-500">
										Búsqueda global
									</label>
								</div>
							)}
						</div>
					</header>
					{withGlobalFilter && <span className="text-gray-600 font-light pl-4 mt-4">{totalRows} Resultado(s)</span>}
					<div className="p-3">
						<div className="overflow-x-auto mb-2">
							<table className="table-auto w-full ">
								<thead className="text-xs uppercase text-gray-700 bg-gradient-to-b from-red-100 to-red-100">
									{tableTanstack.getHeaderGroups().map(headerGroup => (
										<tr key={headerGroup.id}>
											{headerGroup.headers.map(header => (
												<th
													className="p-2 whitespace-nowrap font-normal text-left hover:cursor-pointer relative"
													key={header.id}
													onClick={header.column.getToggleSortingHandler()}>
													<span className="flex flex-nowrap">
														{flexRender(header.column.columnDef.header, header.getContext())}

														<SortedIcon order={header.column.getIsSorted() as SortDirection} />
													</span>
												</th>
											))}
										</tr>
									))}
								</thead>
								<tbody className="text-md divide-y divide-gray-100">
									{tableTanstack.getRowModel().rows.map(row => (
										<tr key={row.id} className={cn('even:bg-rose-100/30 odd:bg-rose-50/20')}>
											{row.getVisibleCells().map(cell => {
												return (
													<td className="p-2 px-1 whitespace-nowrap text-sm" key={cell.id}>
														{flexRender(cell.column.columnDef.cell, cell.getContext())}
													</td>
												);
											})}
										</tr>
									))}
								</tbody>
								<tfoot className="border-b">
									<tr>
										<td className="p-1">
											<IndeterminateCheckbox
												{...{
													checked: tableTanstack.getIsAllPageRowsSelected(),
													indeterminate: tableTanstack.getIsSomePageRowsSelected(),
													onChange: tableTanstack.getToggleAllPageRowsSelectedHandler(),
												}}
											/>
										</td>
										<td colSpan={20}>Viendo ({tableTanstack.getRowModel().rows.length}) filas</td>
									</tr>
								</tfoot>
							</table>
						</div>
						<div className="flex gap-2 sm:gap-4 flex-col sm:flex-row font-light text-gray-600 text-sm">
							{selectedRowsLength > 0 && (
								<ButtonBasic
									className="flex flex-nowrap w-28 h-9 py-0 mt-2 mb-1 ml-auto"
									type="button"
									onClick={() => handleChecksDelete(tableTanstack?.getSelectedRowModel()?.flatRows as Row<T>[])}>
									Eliminar ({selectedRowsLength})
								</ButtonBasic>
							)}

							<div className="flex flex-wrap justify-end gap-3 p-2 items-center w-full">
								{totalRows > 10 && (
									<select
										className="h-9 text-sm rounded-lg py-1 pr-7 border focus:ring-rose-500 focus:border-transparent border-gray-200"
										value={tableTanstack.getState().pagination.pageSize}
										onChange={e => {
											tableTanstack.setPageSize(Number(e.target.value));
										}}>
										{[10, 20, 30, 40, 50].map(pageSize => (
											<option key={pageSize} value={pageSize}>
												Mostrar {pageSize}
											</option>
										))}
									</select>
								)}
								{tableTanstack.getPageCount() > 1 && (
									<>
										<div className="flex flex-nowrap items-center gap-1">
											<button className={pageBtnStyle} onClick={() => tableTanstack.setPageIndex(0)}>
												<ChevronDoubleLeftIcon className="w-5 h-5 text-gray-500" />
											</button>
											<button
												className={cn(
													pageBtnStyle,
													!tableTanstack.getCanPreviousPage() && 'opacity-30 hover:cursor-default active:bg-inherit'
												)}
												disabled={!tableTanstack.getCanPreviousPage()}
												onClick={() => tableTanstack.previousPage()}>
												<ChevronLeftIcon className="w-5 h-5 text-gray-500" />
											</button>
											<button
												className={cn(
													pageBtnStyle,
													!tableTanstack.getCanNextPage() && 'opacity-30 hover:cursor-default active:bg-inherit'
												)}
												onClick={() => tableTanstack.nextPage()}
												disabled={!tableTanstack.getCanNextPage()}>
												<ChevronRightIcon className="w-5 h-5 text-gray-500" />
											</button>
											<button
												className={pageBtnStyle}
												onClick={() => tableTanstack.setPageIndex(tableTanstack.getPageCount() - 1)}>
												<ChevronDoubleRightIcon className="w-5 h-5 text-gray-500" />
											</button>
										</div>
										<span className="ml-2">
											Página {pagination.pageIndex + 1} de {tableTanstack.getPageCount()}
										</span>
									</>
								)}
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>
	);
};

export default TableCustom;
