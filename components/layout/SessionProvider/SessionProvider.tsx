'use client';

import { SessionProvider } from 'next-auth/react';

/**
 * @description Session provider component
 */
export const Provider = ({ children }: { children: React.ReactNode }) => {
	return <SessionProvider>{children}</SessionProvider>;
};
