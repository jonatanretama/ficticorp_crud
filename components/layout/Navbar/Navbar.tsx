'use client';

import { Fragment, useState } from 'react';
import { Dialog, Transition } from '@headlessui/react';
import { Bars3Icon, XMarkIcon } from '@heroicons/react/24/outline';
import Link from 'next/link';
import { PATHS } from '@/const/paths';
import Image from 'next/image';
import { signOut, useSession } from 'next-auth/react';
import { MenuDropdown } from '@/components/molecules/MenuDropdown/MenuDropdown';
import { ButtonBasic } from '@/components/atoms/ButtonBasic/ButtonBasic';
import { usePathname } from 'next/navigation';

/**
 * @description Navbar component
 */
export const Navbar = () => {
	const [mobileMenuOpen, setMobileMenuOpen] = useState(false);
	const { data: session } = useSession();
	const imageUsr = session?.user?.image;
	const nameUsr = session?.user?.name;
	const pathIsAuth = usePathname().includes('/auth/login');

	if (!session || pathIsAuth) return null;

	return (
		<header className="shadow-xl bg-white z-30 sticky top-0 w-full">
			<nav
				className="mx-auto flex max-w-7xl items-center justify-between py-2 px-4 lg:px-8"
				aria-label="Navbar - FictiCorp">
				<div className="flex lg:flex-1">
					<Link href={PATHS.home} className="-m-1.5 p-1">
						<Image className="h-14 w-auto" src="/images/ficticorp700.png" alt="Rifa logo" width={32} height={32} />
					</Link>
				</div>
				<div className="flex lg:hidden">
					<button
						type="button"
						aria-label="Abrir menú de navegación"
						className="-m-2.5 inline-flex items-center justify-center rounded-md p-2.5 text-gray-700"
						onClick={() => setMobileMenuOpen(true)}>
						<Bars3Icon className="h-6 w-6" aria-hidden="true" />
					</button>
				</div>

				<div className="hidden lg:flex lg:flex-1 lg:justify-end">
					<MenuDropdown />
				</div>
			</nav>
			<Transition appear show={mobileMenuOpen} as={Fragment}>
				<Dialog as="div" className="lg:hidden" open={mobileMenuOpen} onClose={setMobileMenuOpen}>
					<Transition.Child
						as={Fragment}
						enter="transform transition ease-out duration-100"
						enterFrom="opacity-0 translate-x-full"
						enterTo="opacity-100 translate-x-0"
						leave="transform transition ease-in duration-200"
						leaveFrom="opacity-100 translate-x-0"
						leaveTo="opacity-0 translate-x-full">
						<Dialog.Panel className="fixed inset-y-0 right-0 z-50 w-full overflow-y-auto bg-white px-4 py-2 sm:max-w-sm sm:ring-1 sm:ring-gray-900/10">
							<div className="flex items-center justify-end">
								<button
									type="button"
									aria-label="Cerrar menú de navegación"
									className="mt-4 mr-2 md:mr-4 rounded-md  text-gray-700 z-50"
									onClick={() => setMobileMenuOpen(false)}>
									<XMarkIcon className="h-6 w-6" aria-hidden="true" />
								</button>
							</div>
							<div className="mt-6 flow-root">
								<div className="-my-6 divide-y divide-gray-500/10">
									<div className="space-y-2 py-3">
										<div className="inline-flex justify-center items-center">
											{imageUsr && (
												<Image
													className="h-8 w-8 rounded-full mr-2"
													src={imageUsr}
													alt="Foto de perfil"
													width={32}
													height={32}
												/>
											)}
											{nameUsr}
										</div>
									</div>
									<div className="py-3 w-full flex justify-center items-center">
										<div>
											<ButtonBasic
												onClick={() => signOut()}
												className="block rounded-lg px-3 py-2.5 text-xl font-semibold leading-7 w-full">
												Cerrar sesión
											</ButtonBasic>
										</div>
									</div>
								</div>
							</div>
						</Dialog.Panel>
					</Transition.Child>
				</Dialog>
			</Transition>
		</header>
	);
};
