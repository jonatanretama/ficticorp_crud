import { Toaster } from 'sonner';

/**
 * @description Custom toast component as a wrapper for the Toaster component (sonner)
 */
export const CustomToast = () => {
	return (
		<Toaster
			toastOptions={{
				unstyled: true,
				classNames: {
					toast: 'px-4 py-3.5 rounded-lg shadow-lg text-sm w-full flex items-center gap-2 text-white',
					title: 'drop-shadow-md',
					description: 'drop-shadow-md',
					error: 'bg-red-500',
					success: 'bg-green-500',
					warning: 'bg-yellow-500',
					info: 'bg-blue-500',
				},
			}}
		/>
	);
};
