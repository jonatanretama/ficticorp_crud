import type { Metadata } from 'next';
import '../globals.css';

export const metadata: Metadata = {
	title: 'FictiCorp',
	description: 'Inicia sesión para acceder a la plataforma de FictiCorp.',
	verification: {
		google: 'DbUUAmpolzqW5tioZVVlojreRGM6D149RfJjBSOKEPA',
	},
};

export default function AuthLayout({ children }: { children: React.ReactNode }) {
	return <div className="min-h-dvh bg-black flex flex-col justify-center items-center w-full">{children}</div>;
}
