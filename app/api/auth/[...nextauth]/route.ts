import NextAuth from 'next-auth';
import GoogleProvider from 'next-auth/providers/google';

const handler = NextAuth({
	providers: [
		GoogleProvider({
			clientId: process.env.NEXT_PUBLIC_GOOGLE_CLIENT_ID as string,
			clientSecret: process.env.NEXT_PUBLIC_GOOGLE_CLIENT_SECRET as string,
			authorization: {
				params: {
					scope: 'openid email profile https://www.googleapis.com/auth/drive',
					// Add the prompt parameter to force the consent screen to appear
					prompt: 'consent',
					access_type: 'offline',
					response_type: 'code',
				},
			},
		}),
	],
	callbacks: {
		async jwt({ token, account }) {
			// Persist the OAuth access_token and or the user id to the token right after signin
			if (account) {
				token.accessToken = account.access_token;
				token.refreshToken = account.refresh_token;
				token.expires = account.expires;
			}
			return token;
		},
		async session({ session, token }) {
			// Add property to session, like an access_token from a provider.
			session.accessToken = token.accessToken as string;
			session.refreshToken = token.refreshToken as string;
			session.expires = token.expires as number;
			return session;
		},
		async redirect({ url, baseUrl }) {
			const redirectUrl = url.startsWith('/') ? new URL(url, baseUrl).toString() : url;
			console.log(`[next-auth] Redirecting to "${redirectUrl}" (resolved from url "${url}" and baseUrl "${baseUrl}")`);
			return redirectUrl;
		},
	},

	pages: {
		signIn: '/auth/login',
	},
});

export { handler as GET, handler as POST };
