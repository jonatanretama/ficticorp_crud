import type { Metadata } from 'next';
import './globals.css';
import { CustomToast } from '@/components/layout/CustomToast/CustomToast';
import { Provider } from '@/components/layout/SessionProvider/SessionProvider';
import { Navbar } from '@/components/layout/Navbar/Navbar';

export const metadata: Metadata = {
	title: 'FictiCorp',
	description:
		'Organización innovadora que se especializa en ofrecer soluciones tecnológicas avanzadas para una variedad de industrias.',
	verification: {
		google: 'DbUUAmpolzqW5tioZVVlojreRGM6D149RfJjBSOKEPA',
	},
};

export default function RootLayout({
	children,
}: Readonly<{
	children: React.ReactNode;
}>) {
	return (
		<html lang="en">
			<body className="min-h-dvh">
				<Provider>
					<Navbar />
					<main>{children}</main>
					<CustomToast />
				</Provider>
			</body>
		</html>
	);
}
