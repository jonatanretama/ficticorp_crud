import { AxiosInstance } from 'axios';

interface IFilePermissions {
	/** Email of the active user */
	email: string;
	/** Axios instance for the Google Drive API */
	driveApi: AxiosInstance;
}

const baseUrl = '/drive/v3/files';

/**
 * @description Service to get the file permissions in Google Drive and set the permissions for the user's email (this should share the file with the user)
 */
export const getFilePermissions = async ({ email, driveApi }: IFilePermissions) => {
	const fileId = process.env.NEXT_PUBLIC_GOOGLE_DRIVE_FILE_ID;
	try {
		await driveApi.request({
			method: 'POST',
			url: `${baseUrl}/${fileId}/permissions`,
			data: {
				role: 'writer',
				type: 'user',
				emailAddress: email,
			},
			params: {
				sendNotificationEmail: false,
			},
		});

		return;
	} catch (error) {
		console.error('Error al obtener los permisos del archivo', error);
	}
};

/**
 * @description Service to get the CSV file from Google Drive
 */
export const getCSVFile = async ({ driveApi }: { driveApi: AxiosInstance }) => {
	const fileId = process.env.NEXT_PUBLIC_GOOGLE_DRIVE_FILE_ID;
	try {
		const response = await driveApi.request({
			method: 'GET',
			url: `${baseUrl}/${fileId}?alt=media`,
		});

		return response.data;
	} catch (error) {
		throw new Error('Error al obtener el archivo CSV, intente cerrar y abrir sesión nuevamente');
	}
};
