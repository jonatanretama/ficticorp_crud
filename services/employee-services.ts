import { employeeJsonToCSV } from '@/adapters/eployeeJsonToCSVAdapter';
import { AxiosInstance } from 'axios';

interface IPatchCSVFile {
	/** Data to update in the CSV file */
	data: any;
	/** Axios instance for the Google Drive API */
	driveApi: AxiosInstance;
}

/**
 * @description Service to update the CSV file in Google Drive
 */
export const patchCSVFile = async ({ data, driveApi }: IPatchCSVFile) => {
	const fileId = process.env.NEXT_PUBLIC_GOOGLE_DRIVE_FILE_ID;
	try {
		const response = await driveApi.request({
			method: 'PATCH',
			url: `upload/drive/v3/files/${fileId}?uploadType=media`,
			data: employeeJsonToCSV(data),
			params: {
				supportsAllDrives: true,
			},
		});

		return response.data;
	} catch (error) {
		throw new Error(`Error al actualizar el archivo CSV: ${error}`);
	}
};
