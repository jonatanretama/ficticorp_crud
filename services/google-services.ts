import axios from 'axios';

/**
 * @description Service to get a new Google oauth2 access token using a refresh token
 */
export const getNewAccessToken = async (refreshToken: string) => {
	try {
		const response = await axios.post(process.env.NEXT_PUBLIC_OAUTH2_TOKEN_URL as string, {
			client_id: process.env.NEXT_PUBLIC_GOOGLE_CLIENT_ID,
			client_secret: process.env.NEXT_PUBLIC_GOOGLE_CLIENT_SECRET,
			refresh_token: refreshToken,
			grant_type: 'refresh_token',
		});

		return response.data.access_token;
	} catch (error) {
		console.error('Error al obtener un nuevo token de acceso:', error);
		throw error;
	}
};
