// eslint-disable-next-line @typescript-eslint/no-unused-vars
import NextAuth from 'next-auth';

declare module 'next-auth' {
	/**
	 * Returned by `useSession`, `getSession` and received as a prop on the `SessionProvider` React Context
	 */
	interface Session {
		/** Access token by auth provider */
		accessToken?: string;
		/** Refresh token by auth provider */
		refreshToken?: string;
		/** Access token expiration timestamp */
		expires?: number;
	}
}
