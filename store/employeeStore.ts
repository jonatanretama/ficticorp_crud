import { defaultEmployee } from '@/data/defaultEmployee';
import { IEmployeesWithDate, IEmployeeWithId } from '@/models/IEmployee';
import { create } from 'zustand';

interface IEmployeeStore {
	/** Actual employee, used to update the employee in the dialog (update) */
	employee: IEmployeeWithId;
	/** Function to edit the current employee */
	editEmployee: (newEmployee: IEmployeeWithId) => void;
	/** Boolean to open dialog */
	isDialogOpen: boolean;
	/** Function to open or close the dialog */
	setDialogOpen: (isOpen: boolean) => void;
	/** All employees */
	allEmployees: IEmployeesWithDate[];
	/** Function to add a new employee into allEmployees */
	postEmployee: (newEmployee: IEmployeesWithDate) => void;
	/** Function to delete an employee from allEmployees */
	deleteEmployee: (id: string) => void;
	/** Function to delete multiple employees from allEmployees */
	deleteEmployeeBulk: (ids: string[]) => void;
	/** Function to update an employee in allEmployees */
	updateEmployee: (updatedEmployee: IEmployeesWithDate) => void;
	/** Function to set all employees */
	setAllEmployees: (employees: IEmployeesWithDate[]) => void;
	/** Boolean to know if the employees are fetched */
	isEmployeesFetched: boolean;
	/** Function to set if the employees are fetched */
	setEmployeesFetched: (isFetched: boolean) => void;
	/** Boolean to know if the data is fetching */
	isFetching: boolean;
	/** Function to set if the data is fetching */
	setFetching: (fetching: boolean) => void;
}

/**
 * @description Zustand store for the employees
 */
export const useEmployeeStore = create<IEmployeeStore>(set => ({
	employee: defaultEmployee,
	editEmployee: currentEmployee => set(state => ({ ...state, employee: currentEmployee })),
	isDialogOpen: false,
	setDialogOpen: isOpen => set(state => ({ ...state, isDialogOpen: isOpen })),
	allEmployees: [],
	postEmployee: newEmployee =>
		set(state => ({
			...state,
			allEmployees: [newEmployee, ...state.allEmployees],
		})),
	deleteEmployee: (id: string) =>
		set(state => ({
			...state,
			allEmployees: state.allEmployees.filter(employee => employee.id !== id),
		})),
	deleteEmployeeBulk: (ids: string[]) =>
		set(state => ({
			...state,
			allEmployees: state.allEmployees.filter(employee => !ids.includes(employee.id)),
		})),
	updateEmployee: updatedEmployee =>
		set(state => ({
			allEmployees: state.allEmployees
				.map(employee => (employee.id === updatedEmployee.id ? updatedEmployee : employee))
				.filter(employee => employee.updatedAt !== undefined)
				.sort((a, b) => {
					const dateA = new Date(a.updatedAt!);
					const dateB = new Date(b.updatedAt!);
					return dateB.getTime() - dateA.getTime();
				}),
		})),
	setAllEmployees: (employees: IEmployeesWithDate[]) =>
		set(state => ({
			...state,
			allEmployees: employees
				.filter(employee => employee.updatedAt !== undefined)
				.sort((a, b) => {
					const dateA = new Date(a.updatedAt!);
					const dateB = new Date(b.updatedAt!);
					return dateB.getTime() - dateA.getTime();
				}),
		})),
	isEmployeesFetched: false,
	setEmployeesFetched: (isFetched: boolean) => set(state => ({ ...state, isEmployeesFetched: isFetched })),
	isFetching: true,
	setFetching: (fetching: boolean) => set(state => ({ ...state, isFetching: fetching })),
}));
