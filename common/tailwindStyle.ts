import { cn } from '@/utils/cn';
import { ClassValue } from 'clsx';

export const styleErrorSpan = (...inputs: ClassValue[]) => cn('text-red-500 text-xs italic', inputs);

export const styleInput = (...inputs: ClassValue[]) =>
	cn(
		'border-[1px] border-gray-200 focus:border-transparent rounded-lg w-full py-2 px-3 text-gray-700 mb-0 leading-tight focus:outline-none focus:ring-2 focus:ring-rose-400',
		inputs
	);
