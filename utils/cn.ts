import { ClassValue, clsx } from 'clsx';
import { twMerge } from 'tailwind-merge';

/**
 * A utility function to merge classnames with tailwindcss classes.
 * @param inputs - The classnames to merge.
 * @returns The merged classnames.
 */
export function cn(...inputs: ClassValue[]) {
	return twMerge(clsx(inputs));
}
