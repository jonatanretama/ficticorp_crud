import axios from 'axios';

/**
 * @description Axios instance for the Google Drive API
 */
const driveBase = axios.create({
	baseURL: process.env.NEXT_PUBLIC_GOOGLE_DRIVE_BASE_URL,
});

export const googleDriveInstance = driveBase;
