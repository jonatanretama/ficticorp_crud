import { AllowedAreasEnum } from '@/models/AllowedAreasEnum';
import { AllowedChargesEnum } from '@/models/AllowedChargesEnum';
import { ISelectOption } from '@/models/IEmployee';

/**
 * @description Convert an enum to an array of ISelectOption
 */
const convertEnumToArray = (enumObject: any): ISelectOption[] => {
	const enumKeys = Object.keys(enumObject).filter(key => typeof enumObject[key as any] === 'string');
	return enumKeys.map(key => ({ id: key, name: enumObject[key] }));
};

export const areasOptions = convertEnumToArray(AllowedAreasEnum);

export const chargesOptions = convertEnumToArray(AllowedChargesEnum);
