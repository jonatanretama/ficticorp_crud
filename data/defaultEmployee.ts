import { AllowedAreasEnum } from '@/models/AllowedAreasEnum';
import { AllowedChargesEnum } from '@/models/AllowedChargesEnum';

/**
 * @description Default employee object
 * Used to reset the form and default value for zustand
 */
export const defaultEmployee = {
	id: '',
	name: '',
	paternalSurname: '',
	maternalSurname: '',
	email: '',
	phone: '',
	charge: { id: 'default', name: 'Seleccionar Puesto' as unknown as AllowedChargesEnum },
	area: { id: 'default', name: 'Seleccionar Área' as unknown as AllowedAreasEnum },
	salary: '',
};
