'use client';

import { googleDriveInstance } from '@/utils/googleDriveInstance';
import { useSession } from 'next-auth/react';
import { useEffect } from 'react';
import { useRefreshToken } from './useRefreshToken';

/**
 * @description Hook to use the google drive api
 * @returns google drive instance
 * Here the authorization token is added to the request headers by interceptors
 * If the request returns a 401 status code, the refresh token is used to get a new access token
 */
export const useGoogleDriveAPI = () => {
	const { data: session } = useSession();
	const refreshToken = useRefreshToken();

	useEffect(() => {
		const requestInterceptor = googleDriveInstance.interceptors.request.use(
			config => {
				if (!config.headers['Authorization']) {
					config.headers['Authorization'] = `Bearer ${session?.accessToken}`;
				}
				return config;
			},
			error => Promise.reject(error)
		);

		const responseInterceptor = googleDriveInstance.interceptors.response.use(
			response => response,
			async error => {
				const prevRequest = error.config;
				if (error.response?.status === 401 && !prevRequest.sent) {
					prevRequest.sent = true;
					// Refresh the token and retry the request if necessary
					await refreshToken();
					prevRequest.headers['Authorization'] = `Bearer ${session?.accessToken}`;
					return googleDriveInstance(prevRequest);
				}
				return Promise.reject(error);
			}
		);

		return () => {
			googleDriveInstance.interceptors.request.eject(requestInterceptor);
			googleDriveInstance.interceptors.response.eject(responseInterceptor);
		};
	}, [session]);

	return googleDriveInstance;
};
