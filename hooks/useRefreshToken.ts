import { getNewAccessToken } from '@/services/google-services';
import { useSession } from 'next-auth/react';

/**
 * @description Hook to refresh the token
 * @returns refresh token string
 */
export const useRefreshToken = () => {
	const { data: session } = useSession();

	const refreshToken = async () => {
		const response = await getNewAccessToken(session?.refreshToken as string);

		if (session) session.accessToken = response;
	};

	return refreshToken;
};
