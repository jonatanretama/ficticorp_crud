'use client';

import { csvToEmployeeAdapter } from '@/adapters/csvToEmployeeAdapter';
import { columnsTableEmployees } from '@/components/molecules/TableEmployees/columnsTableEmployees';
import { IEmployeesWithDate } from '@/models/IEmployee';
import { getCSVFile } from '@/services/drive-services';
import { patchCSVFile } from '@/services/employee-services';
import { useEmployeeStore } from '@/store/employeeStore';
import { Row, ColumnDef } from '@tanstack/react-table';
import { useSession } from 'next-auth/react';
import { useEffect } from 'react';
import { toast } from 'sonner';
import { useGoogleDriveAPI } from './useDriveInstance';

/**
 * @description Hook to use the employees table
 * @returns columns and delete employee bulk function
 */
export const useTableEmployees = () => {
	const {
		allEmployees,
		setEmployeesFetched,
		setDialogOpen,
		editEmployee,
		deleteEmployee,
		deleteEmployeeBulk,
		setAllEmployees,
		setFetching,
	} = useEmployeeStore();
	const { data: session } = useSession();

	const driveApi = useGoogleDriveAPI();

	// Fetch the CSV file from google drive, when starts fetching a spinner is activated, needs the access token from the session
	const fetchCSV = async () => {
		setFetching(true);
		await getCSVFile({ driveApi })
			.then(data => {
				setAllEmployees(csvToEmployeeAdapter(data));
				setEmployeesFetched(true);
			})
			.catch(error => {
				toast.error(error.message);
			})
			.finally(() => {
				setFetching(false);
			});
	};

	useEffect(() => {
		if (session?.accessToken) {
			fetchCSV();
		}
	}, [session?.accessToken]);

	const handleOpenModal = () => {
		setDialogOpen(true);
	};

	// Delete employees in bulk, needs the data of the employees to delete
	const handleDeleteEmployeeBulk = async (data: Row<IEmployeesWithDate>[]) => {
		setFetching(true);
		const ids: string[] = data.map(row => row.original.id);

		const withoutDeletedEmployees = allEmployees.filter(e => !ids.includes(e.id));

		await patchCSVFile({ data: withoutDeletedEmployees, driveApi })
			.then(response => {
				if (response?.name) {
					deleteEmployeeBulk(ids);
					toast.success(`${ids.length} Empleados eliminados`);
				}
			})
			.catch(error => {
				toast.error('Error al eliminar empleados', { description: error });
			})
			.finally(() => {
				setFetching(false);
			});
	};

	// Delete one employee, needs the employee to delete
	const handleDeleteOneEmployee = async (employee: IEmployeesWithDate) => {
		setFetching(true);
		const withoutEmployee = allEmployees.filter(e => e.id !== employee.id);
		await patchCSVFile({ data: withoutEmployee, driveApi })
			.then(response => {
				if (response?.name) {
					deleteEmployee(employee.id);
					toast.success('Empleado eliminado', {
						description: `${employee.name} ${employee.paternalSurname} ${employee.maternalSurname}`,
					});
				}
			})
			.catch(error => {
				toast.error('Error al eliminar empleado', { description: error });
			})
			.finally(() => {
				setFetching(false);
			});
	};

	// Columns for the table, needs the functions to open the modal, edit and delete employees. Used by react table of tanstack
	const columns = columnsTableEmployees({ handleOpenModal, editEmployee, handleDeleteOneEmployee }) as ColumnDef<
		unknown,
		any
	>[];

	return { columns, handleDeleteEmployeeBulk };
};
